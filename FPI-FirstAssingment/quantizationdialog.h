#ifndef QUANTIZATIONDIALOG_H
#define QUANTIZATIONDIALOG_H

#include <QDialog>

namespace Ui {
class QuantizationDialog;
}

class QuantizationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QuantizationDialog(QWidget *parent = 0);
    ~QuantizationDialog();

private:
    Ui::QuantizationDialog *ui;
};

#endif // QUANTIZATIONDIALOG_H
