#ifndef IMAGEFILE_H
#define IMAGEFILE_H

#include <QObject>
#include <QImage>

#include "convolutionkernel.h"

typedef enum
{
    right,
    left
} RotationDirection;


typedef unsigned char pixel;

class ImageFile : public QObject
{
    Q_OBJECT
public:
    explicit ImageFile(QObject *parent = 0);

    QString getFileName();
    QImage *getImageBytes();
    int getPixelLength();
    QPixmap getPixmap();
    void printHistogram();
    uchar* getHistogram();

    bool loadImage();
    bool loadImage(QString _fileName);
    bool loadImage(QImage _image);
    void setFileName(QString name);
    bool saveFile();
    bool saveAs(QString _fileName);
    void setPixelLength(int l);

    void horizontalMirroring();
    void verticalMirroring();
    void calculateLuminance();
    uchar pixelQuantization(int shades, uchar pixel);
    void imageQuantization(int shades);
    void computeNegative();
    void zoomIn();
    void zoomOut();

    void rotate(RotationDirection direction);

    void setBrightness(int scalar);
    void setContrast(float scalar);

    void setConvolutionKernel(ConvolutionKernel* convkernel);
    void doConvolution();

private:
    QString fileName;
    QImage* imagebytes;
    int pixelLength;
    uchar* histogram;
    ConvolutionKernel* kernel = NULL;

    void computeHistogram();
    QImage* convolution();
    uchar checkOverflowOrNegativeValues(float s, KernelFIlters filter);

signals:

public slots:
};

#endif // IMAGEFILE_H
