#ifndef CONVOLUTIONKERNEL_H
#define CONVOLUTIONKERNEL_H

#include <QObject>

typedef enum
{
    Gaussian,
    Laplacian,
    GenericHighpass,
    PrewittHx,
    PrewittHyHx,
    SobelHx,
    SobelHy
} KernelFIlters;

class ConvolutionKernel : public QObject
{
    Q_OBJECT
public:
    explicit ConvolutionKernel(QObject *parent = 0, int nrows = 3, int ncolumns = 3);
    void setValuePosition(int srow, int scolumn, float value);
    float getValuePosition(int grow, int gcolumn);
    KernelFIlters getFilterType();
    void setFilterType(KernelFIlters f);

    int getRows();
    int getColumns();
    void rotate180();
    void resetRotate();

    int printKernel();

private:
    float** kernel;
    float** original;
    int rows, columns;
    KernelFIlters filter;

signals:

public slots:
};

#endif // CONVOLUTIONKERNEL_H
