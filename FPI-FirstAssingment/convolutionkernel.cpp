#include "convolutionkernel.h"

ConvolutionKernel::ConvolutionKernel(QObject *parent, int nrows, int ncolumns) : QObject(parent)
{
    rows = nrows;
    columns = ncolumns;
    kernel = (float**)calloc(nrows , sizeof(float*));

    for(int r = 0; r < nrows; r++)
        kernel[r] = (float*)calloc(ncolumns , sizeof(float));
}

void ConvolutionKernel::setValuePosition(int srow, int scolumn, float value)
{
    if(srow > getRows() || scolumn > getColumns())
        return;

    if(srow < 0 || scolumn < 0)
        return;

    kernel[srow][scolumn] = value;
}

float ConvolutionKernel::getValuePosition(int grow, int gcolumn)
{
    if(grow > getRows() || gcolumn > getColumns()) //Incorrect index
        return std::numeric_limits<float>::max();

    if(grow < 0 || gcolumn < 0)
        return std::numeric_limits<float>::max(); //Incorrect index

    return kernel[grow][gcolumn];
}

KernelFIlters ConvolutionKernel::getFilterType()
{
    return filter;
}

void ConvolutionKernel::setFilterType(KernelFIlters f)
{
    filter = f;
}

int ConvolutionKernel::getRows()
{
    return rows;
}

int ConvolutionKernel::getColumns()
{
    return columns;
}

void ConvolutionKernel::rotate180()
{
    float** rotated = (float**)calloc(getRows(), sizeof(float*));

    for(int r = 0; r < getRows(); r++)
        rotated[r] = (float*)calloc(getColumns(), sizeof(float));

    //Invert the rows
    for(int r = 0; r < getRows(); r++)
        memcpy(rotated[getRows() - r - 1], kernel[r], sizeof(float)*getColumns());

    float* rowArray = (float*)calloc(getColumns(), sizeof(float));

    for(int r = 0; r < getRows(); r++)
    {
        for(int c = 0; c < getColumns(); c++)
        {
            rowArray[getColumns() - c - 1] = rotated[r][c];
        }
        memcpy(rotated[r], rowArray, sizeof(float)*getColumns());
    }
    original = kernel;
    kernel = rotated;
}

void ConvolutionKernel::resetRotate()
{
    free(kernel);
    kernel = original;
}

int ConvolutionKernel::printKernel()
{
    qDebug("Kernel");
    for(int r = 0; r < getRows(); r++)
        for(int c = 0; c < getColumns(); c++)
            qDebug("[%i][%i] -> %f", r, c, kernel[r][c]);
}
