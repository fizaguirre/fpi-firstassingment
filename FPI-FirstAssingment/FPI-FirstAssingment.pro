#-------------------------------------------------
#
# Project created by QtCreator 2016-08-21T14:18:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FPI-FirstAssingment
TEMPLATE = app


SOURCES += main.cpp\
        controlwindow.cpp \
    imagefile.cpp \
    imageviewer.cpp \
    quantizationdialog.cpp \
    histogramview.cpp \
    kernelview.cpp \
    convolutionkernel.cpp

HEADERS  += controlwindow.h \
    imagefile.h \
    imageviewer.h \
    quantizationdialog.h \
    histogramview.h \
    kernelview.h \
    convolutionkernel.h

FORMS    += controlwindow.ui \
    imageviewer.ui \
    quantizationdialog.ui \
    histogramview.ui \
    kernelview.ui
