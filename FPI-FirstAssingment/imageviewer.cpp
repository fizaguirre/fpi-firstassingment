#include "imageviewer.h"
#include "ui_imageviewer.h"

ImageViewer::ImageViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ImageViewer)
{
    ui->setupUi(this);
}

ImageViewer::~ImageViewer()
{
    delete ui;
}

void ImageViewer::setImage(ImageFile* _image)
{
    image = _image;
}

void ImageViewer::displayImage()
{
    //const QImage temp = image->getImageBytes();
    //ui->imageLabel->setPixmap(QPixmap::fromImage(image->getImageBytes()).scaled(QWidget::width(),QWidget::height(),Qt::KeepAspectRatio));
    //ui->imageLabel->resize(0.25 * ui->imageLabel->pixmap()->size());

    //ui->imageLabel->setPixmap(QPixmap::fromImage(image->getImageBytes()));
    ui->imageLabel->setPixmap(image->getPixmap());
    ui->imageLabel->adjustSize();
    QWidget::setWindowTitle(image->getFileName());
}
