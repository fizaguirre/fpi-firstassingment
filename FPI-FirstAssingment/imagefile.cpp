#include "imagefile.h"
#include <QImageReader>
#include <QPixmap>


ImageFile::ImageFile(QObject *parent) : QObject(parent)
{
    fileName = "";
    imagebytes = new QImage;
}


QString ImageFile::getFileName()
{
    return fileName;
}

QImage* ImageFile::getImageBytes()
{
    return imagebytes;
}

QPixmap ImageFile::getPixmap()
{
    return QPixmap::fromImage(*imagebytes);
}

void ImageFile::setFileName(QString name)
{
    fileName = name;
}

bool ImageFile::loadImage()
{
    QImageReader reader(getFileName());
    reader.setAutoTransform(true);
    *imagebytes = reader.read();
    setPixelLength(imagebytes->bytesPerLine()/imagebytes->width());

    if(imagebytes->isNull())
        return false;
    return true;
}

bool ImageFile::loadImage(QString _fileName)
{
    setFileName(_fileName);
    QImageReader reader(getFileName());
    reader.setAutoTransform(true);
    *imagebytes = reader.read();

    if(imagebytes->isNull())
        return false;

    setPixelLength(imagebytes->bytesPerLine()/imagebytes->width());
    return true;
}

bool ImageFile::loadImage(QImage _image)
{
    //TODO
    return true;
}

bool ImageFile::saveFile()
{
    return imagebytes->save(getFileName(), "JPG");
}

bool ImageFile::saveAs(QString _fileName)
{
    return imagebytes->save(_fileName, "JPG");
}

void ImageFile::horizontalMirroring()
{
    qDebug("Bytes per Line %i - Width: %i - Height: %i" ,imagebytes->bytesPerLine(), imagebytes->width(), imagebytes->height());

    uchar** mirrorData = (uchar**)calloc(imagebytes->height(), sizeof(uchar*));

    for(int height = 0; height < imagebytes->height(); height++)
    {
        mirrorData[height] = (uchar*)calloc(imagebytes->bytesPerLine(), sizeof(uchar));
        memcpy(mirrorData[height], imagebytes->scanLine(height), imagebytes->bytesPerLine());
    }

    for(int mirrorHeight = (imagebytes->height() - 1), oHeight = 0; mirrorHeight >= 0; mirrorHeight--, oHeight++)
        memcpy(imagebytes->scanLine(oHeight), mirrorData[mirrorHeight], imagebytes->bytesPerLine());

    free(mirrorData);

}

void ImageFile::verticalMirroring()
{
    qDebug("Bytes per Line %i - Width: %i - Height: %i" ,imagebytes->bytesPerLine(), imagebytes->width(), imagebytes->height());

    uchar** mirrorData = (uchar**)calloc(imagebytes->height(), sizeof(uchar*));

    for(int height = 0; height < imagebytes->height(); height++)
    {
        mirrorData[height] = (uchar*)calloc(imagebytes->bytesPerLine(), sizeof(uchar));
        memcpy(mirrorData[height], imagebytes->scanLine(height), imagebytes->bytesPerLine());
    }

    int pixelWidth = imagebytes->bytesPerLine()/imagebytes->width();
    qDebug("Pixel Width: %i", pixelWidth);
    for(int oHeight = 0; oHeight < imagebytes->height(); oHeight++)
    {
        for(int oWidth = 0, mWidth = imagebytes->bytesPerLine() - pixelWidth, pixel = 0; pixel < imagebytes->width(); oWidth += pixelWidth, mWidth -= pixelWidth, pixel++)
            memcpy(imagebytes->scanLine(oHeight) + oWidth, &mirrorData[oHeight][mWidth], pixelWidth);
    }



    free(mirrorData);
}

void ImageFile::calculateLuminance()
{
    float lumin = 0;
    int pixelLengh = getPixelLength(); //The lengh in bytes of a pixel in memory
    for(int i = 0; i < imagebytes->height(); i++)
    {
        for(int j = 0; j < imagebytes->width(); j++)
        {
            lumin += *(imagebytes->scanLine(i) + j*pixelLengh)*0.299; //R
            lumin += *(imagebytes->scanLine(i) + j*pixelLengh + 1)*0.587; //G
            lumin += *(imagebytes->scanLine(i) + j*pixelLengh + 2)*0.114; //B

            *(imagebytes->scanLine(i) + j*pixelLengh) = (uchar)lumin;
            *(imagebytes->scanLine(i) + j*pixelLengh + 1) = (uchar)lumin;
            *(imagebytes->scanLine(i) + j*pixelLengh + 2) = (uchar)lumin;

            lumin = 0;
        }
    }
}

void ImageFile::setPixelLength(int l)
{
    pixelLength = l;
}

int ImageFile::getPixelLength()
{
    return pixelLength;
}

void ImageFile::computeHistogram()
{
    calculateLuminance();

    uchar* hist = (uchar*)calloc(255, sizeof(uchar));

    for(int i = 0; i < imagebytes->height(); i++)
        for(int j = 0; j < imagebytes->width(); j++)
            hist[*(imagebytes->scanLine(i) + j * getPixelLength())]++;

    histogram = hist;
}

QImage *ImageFile::convolution()
{
    QImage* convolutionImage = new QImage(imagebytes->width(), imagebytes->height(), imagebytes->format());
    convolutionImage->fill(QColor(Qt::black).toRgb());

    int n2, m2;
    float sum = 0;

    n2 = floor(kernel->getColumns()/2);
    m2 = floor(kernel->getRows()/2);

    for(int y = n2; y < (imagebytes->width() - n2 - 1); y++)
        for(int x = m2; x < (imagebytes->height() - m2 -1); x++)
        {
            sum = 0.0;
            for(int k = -n2; k <= n2; k++)
            {
                for(int j = -m2; j <= m2; j++)
                    sum += kernel->getValuePosition(j+m2, k+n2) * (float)imagebytes->scanLine(x-j)[(y-k)*getPixelLength()];
                convolutionImage->scanLine(x)[y*getPixelLength()] = checkOverflowOrNegativeValues(sum,kernel->getFilterType());
                convolutionImage->scanLine(x)[y*getPixelLength()+1] = convolutionImage->scanLine(x)[y*getPixelLength()];
                convolutionImage->scanLine(x)[y*getPixelLength()+2] = convolutionImage->scanLine(x)[y*getPixelLength()];
            }
        }
    return convolutionImage;
}

uchar ImageFile::checkOverflowOrNegativeValues(float s, KernelFIlters filter)
{
    if(filter != Gaussian && filter != GenericHighpass)
        s += 127.0;

    if(s > 255)
        return (uchar)255;
    else if(s < 0)
        return (uchar)0;

    return (uchar)s;
}

void ImageFile::printHistogram()
{
    qDebug("Histogram");
    for(int i = 0; i < 255; i++)
        qDebug("[%i] = %i", i, histogram[i]);
}

uchar *ImageFile::getHistogram()
{
    computeHistogram();
    return histogram;
}

uchar ImageFile::pixelQuantization(int shades, uchar pixel)
{
    if(shades < 1 && shades > 255)
        return 0;

    int div = 255 / shades;
    int vpixel = (int)pixel;

    return (uchar)floor(vpixel - vpixel%div);

}

void ImageFile::imageQuantization(int shades)
{
    calculateLuminance();

    int vpixel = 0;
    for(int i = 0; i < imagebytes->height(); i++)
    {
        for(int j = 0; j < imagebytes->width(); j++)
        {
            vpixel = pixelQuantization(shades, *(imagebytes->scanLine(i) + j * getPixelLength()));
            *(imagebytes->scanLine(i) + j * getPixelLength()) = vpixel;
            *(imagebytes->scanLine(i) + j * getPixelLength() + 1) = vpixel;
            *(imagebytes->scanLine(i) + j * getPixelLength() + 2) = vpixel;
        }
    }
}

void ImageFile::computeNegative()
{
    int index = 0;
    for(int height = 0; height < imagebytes->height(); height++)
        for(int width = 0; width < imagebytes->width(); width++)
        {
            index = width*getPixelLength();
            imagebytes->scanLine(height)[index] = (uchar)255 - imagebytes->scanLine(height)[index];
            imagebytes->scanLine(height)[index+1] = (uchar)255 - imagebytes->scanLine(height)[index+1];
            imagebytes->scanLine(height)[index+2] = (uchar)255 - imagebytes->scanLine(height)[index+2];
        }
    qDebug("teste");
}

void ImageFile::zoomIn()
{
    QImage* zoomedImage = new QImage(imagebytes->width()*2, imagebytes->height()*2, imagebytes->format());
    zoomedImage->fill(QColor(Qt::black).toRgb());
    int indexZoom = 0, indexOriginal = 0;

    //Copy the original image to the new image
    for(int row = 0, rowOriginal = 0; row < zoomedImage->height(); row += 2, rowOriginal++)
        for(int column = 0, columnOriginal = 0; column < zoomedImage->width(); column += 2, columnOriginal++)
        {
            indexZoom = column * getPixelLength();
            indexOriginal = columnOriginal * getPixelLength();

            zoomedImage->scanLine(row)[indexZoom] = imagebytes->scanLine(rowOriginal)[indexOriginal];
            zoomedImage->scanLine(row)[indexZoom+1] = imagebytes->scanLine(rowOriginal)[indexOriginal+1];
            zoomedImage->scanLine(row)[indexZoom+2] = imagebytes->scanLine(rowOriginal)[indexOriginal+2];
        }


    int pixelBefore = 0, pixelNext = 0;
    for(int row = 0; row < zoomedImage->height(); row += 2)
        for(int column = 1; column < zoomedImage->width(); column += 2)
        {
            indexZoom = column * getPixelLength();
            pixelBefore = indexZoom - getPixelLength();
            pixelNext = indexZoom + getPixelLength();

            zoomedImage->scanLine(row)[indexZoom] = (uchar)(((uint)zoomedImage->scanLine(row)[pixelBefore] + (uint)zoomedImage->scanLine(row)[pixelNext])/(uint)2);
            zoomedImage->scanLine(row)[indexZoom+1] = (uchar)(((uint)zoomedImage->scanLine(row)[pixelBefore+1] + (uint)zoomedImage->scanLine(row)[pixelNext+1])/(uint)2);
            zoomedImage->scanLine(row)[indexZoom+2] = (uchar)(((uint)zoomedImage->scanLine(row)[pixelBefore+2] + (uint)zoomedImage->scanLine(row)[pixelNext+2])/(uint)2);
        }

    for(int row = 1; row < zoomedImage->height(); row += 2)
        for(int column = 0; column < zoomedImage->width(); column++)
        {
            indexZoom = column * getPixelLength();

            zoomedImage->scanLine(row)[indexZoom] = (uchar)(((uint)zoomedImage->scanLine(row-1)[indexZoom] + (uint)zoomedImage->scanLine(row+1)[indexZoom])/(uint)2);
            zoomedImage->scanLine(row)[indexZoom+1] = (uchar)(((uint)zoomedImage->scanLine(row-1)[indexZoom+1] + (uint)zoomedImage->scanLine(row+1)[indexZoom+1])/(uint)2);
            zoomedImage->scanLine(row)[indexZoom+2] = (uchar)((uint)zoomedImage->scanLine(row-1)[indexZoom+2] + (uint)zoomedImage->scanLine(row+1)[indexZoom+2])/2;

            /*
            zoomedImage->scanLine(row)[indexZoom] = (uchar)(((long)zoomedImage->scanLine(row-1)[indexZoom - getPixelLength()] + (long)zoomedImage->scanLine(row+1)[indexZoom - getPixelLength()]
                    + (long)zoomedImage->scanLine(row-1)[indexZoom + getPixelLength()] + (long)zoomedImage->scanLine(row+1)[indexZoom + getPixelLength()])/(long)4);
            zoomedImage->scanLine(row)[indexZoom+1] = (uchar)(((long)zoomedImage->scanLine(row-1)[indexZoom+1 - getPixelLength()] + (long)zoomedImage->scanLine(row+1)[indexZoom+1 - getPixelLength()]
                    + (long)zoomedImage->scanLine(row-1)[indexZoom+1 + getPixelLength()] + (long)zoomedImage->scanLine(row+1)[indexZoom+1 + getPixelLength()])/(long)4);
            zoomedImage->scanLine(row)[indexZoom+2] = (uchar)(((long)zoomedImage->scanLine(row-1)[indexZoom+2 - getPixelLength()] + (long)zoomedImage->scanLine(row+1)[indexZoom+2 - getPixelLength()]
                    + (long)zoomedImage->scanLine(row-1)[indexZoom+2 + getPixelLength()] + (long)zoomedImage->scanLine(row+1)[indexZoom+2 + getPixelLength()])/(long)4);
            */
        }

    free(imagebytes);
    imagebytes = zoomedImage;
}

void ImageFile::zoomOut()
{
    int sx, sy;

    sx = sy = 2;

    QImage* newImage = new QImage(imagebytes->width()/sx, imagebytes->height()/sy, imagebytes->format());


    uint pixelR , pixelG , pixelB;
    pixelR = pixelG = pixelB = 0;
    for(int r = 0; r < newImage->height(); r++)
    {
        for(int c = 0; c < newImage->width(); c++)
        {
            for(int pixelColumn = c*sx; pixelColumn < c*sx+sx; pixelColumn++)
            {
                for(int pixelRow = r*sy; pixelRow < r*sy+sy; pixelRow++)
                {
                    pixelR += imagebytes->scanLine(pixelRow)[pixelColumn*getPixelLength()];
                    pixelG += imagebytes->scanLine(pixelRow)[pixelColumn*getPixelLength()+1];
                    pixelB += imagebytes->scanLine(pixelRow)[pixelColumn*getPixelLength()+2];
                }
            }

            pixelR = pixelR / (sx * sy);
            if(pixelR > 255)
                newImage->scanLine(r)[c*getPixelLength()] = (uchar)255;
            else newImage->scanLine(r)[c*getPixelLength()] = (uchar)pixelR;

            pixelG = pixelG / (sx * sy);
            if(pixelG > 255)
                newImage->scanLine(r)[c*getPixelLength()+1] = (uchar)255;
            else newImage->scanLine(r)[c*getPixelLength()+1] = (uchar)pixelG;

            pixelB = pixelB / (sx * sy);
            if(pixelB > 255)
                newImage->scanLine(r)[c*getPixelLength()+2] = (uchar)255;
            else newImage->scanLine(r)[c*getPixelLength()+2] = (uchar)pixelB;

            pixelR = pixelG = pixelB = 0;
        }
    }

    free(imagebytes);
    imagebytes = newImage;

}

void ImageFile::rotate(RotationDirection direction)
{
    QImage* rotated = new QImage(imagebytes->height(), imagebytes->width(), imagebytes->format());
    rotated->fill(QColor(Qt::black).toRgb());

    int index = 0, indexRotated = 0;
    for(int row = 0; row < imagebytes->height(); row++)
        for(int column = 0; column < imagebytes->width(); column++)
        {
            index = column * getPixelLength();
            indexRotated = row * getPixelLength();

            rotated->scanLine(column)[indexRotated] = imagebytes->scanLine(row)[index];
            rotated->scanLine(column)[indexRotated+1] = imagebytes->scanLine(row)[index+1];
            rotated->scanLine(column)[indexRotated+2] = imagebytes->scanLine(row)[index+2];
        }

    free(imagebytes);
    imagebytes = rotated;

    if(direction == right)
        verticalMirroring();
    else
        horizontalMirroring();
}

void ImageFile::setBrightness(int scalar)
{
    int pixelLengh = getPixelLength(); //The lengh in bytes of a pixel in memory
    for(int i = 0; i < imagebytes->height(); i++)
    {
        for(int j = 0; j < imagebytes->width(); j++)
        {
            if(scalar >= 0)
            {
                for(int index = 0; index <= 2; index++)
                {
                    if((int)*(imagebytes->scanLine(i) + j*pixelLengh + index) + scalar > 255)
                        *(imagebytes->scanLine(i) + j*pixelLengh + index) = 255;
                    else *(imagebytes->scanLine(i) + j*pixelLengh + index) = *(imagebytes->scanLine(i) + j*pixelLengh + index) + (uchar)scalar;
                }

            }
            else
            {
                for(int index = 0; index <= 2; index++)
                {
                    if((int)*(imagebytes->scanLine(i) + j*pixelLengh + index) + scalar < 0)
                        *(imagebytes->scanLine(i) + j*pixelLengh + index) = 0;
                    else *(imagebytes->scanLine(i) + j*pixelLengh + index) = *(imagebytes->scanLine(i) + j*pixelLengh + index) - (uchar)(scalar * -1);

                }
            }
        }
    }
}

void ImageFile::setContrast(float scalar)
{
    int pixelLengh = getPixelLength(); //The lengh in bytes of a pixel in memory
    for(int i = 0; i < imagebytes->height(); i++)
    {
        for(int j = 0; j < imagebytes->width(); j++)
        {
            if(scalar >= 0)
            {
                for(int index = 0; index <= 2; index++)
                {
                    if((float)*(imagebytes->scanLine(i) + j*pixelLengh + index) * scalar > 255)
                        *(imagebytes->scanLine(i) + j*pixelLengh + index) = 255;
                    else *(imagebytes->scanLine(i) + j*pixelLengh + index) = (uchar)((float)*(imagebytes->scanLine(i) + j*pixelLengh + index) * scalar);
                }

            }
            else
            {
                for(int index = 0; index <= 2; index++)
                {
                    if((float)*(imagebytes->scanLine(i) + j*pixelLengh + index) * scalar < 0)
                        *(imagebytes->scanLine(i) + j*pixelLengh + index) = 0;
                    else *(imagebytes->scanLine(i) + j*pixelLengh + index) = (uchar)((float)*(imagebytes->scanLine(i) + j*pixelLengh + index) * (scalar * -1));

                }
            }
        }
    }
}

void ImageFile::setConvolutionKernel(ConvolutionKernel *convkernel)
{
    kernel = convkernel;
}

void ImageFile::doConvolution()
{
    if(kernel != NULL)
    {
        QImage* convolutioned;

        kernel->rotate180();
        kernel->printKernel();

        convolutioned = convolution();
        free(imagebytes);
        imagebytes = convolutioned;

        kernel->resetRotate();
    }
}
