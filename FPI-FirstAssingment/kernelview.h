#ifndef KERNELVIEW_H
#define KERNELVIEW_H

#include <QDialog>
#include <QAbstractButton>

#include "convolutionkernel.h"

namespace Ui {
class KernelView;
}

class KernelView : public QDialog
{
    Q_OBJECT

public:
    explicit KernelView(QWidget *parent = 0);
    ~KernelView();

    ConvolutionKernel* getKernel();

private slots:
    void on_convolutionButtonBox_clicked(QAbstractButton *button);

    void on_kernelsListComboBox_currentIndexChanged(const QString &arg1);

signals:
    void kernelSet(ConvolutionKernel* convkernel);

private:
    Ui::KernelView *ui;

    ConvolutionKernel* dialogKernel;

    void setKernelFilter(QString selectedItem);
};

#endif // KERNELVIEW_H
