#include "controlwindow.h"
#include "ui_controlwindow.h"

#include <QFileDialog>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QStandardPaths>
#include <QIcon>
#include <QSpinBox>

#include "quantizationdialog.h"
#include "histogramview.h"
#include "kernelview.h"

ControlWindow::ControlWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ControlWindow)
{
    ui->setupUi(this);

    originalImage = new ImageFile;
    copyImage = new ImageFile;

    //originalView = new ImageViewer;

    createActions();
    createMenus();

    /*
    QWidget* widget = new QWidget;
    setCentralWidget(widget);

    statusLabel = new QLabel(tr("Chose a menu option"));
    statusLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    statusLabel->setAlignment(Qt::AlignCenter);

    QWidget* bottomFiller = new QWidget;
    bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setMargin(2);
    layout->addWidget(statusLabel);
    layout->addWidget(bottomFiller);
    widget->setLayout(layout);
    */
}

ControlWindow::~ControlWindow()
{
    delete ui;
}

void ControlWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openFileAct);
    fileMenu->addAction(saveFileAct);
    fileMenu->addAction(saveFileAsAct);
    fileMenu->addAction(closeFileAct);
    fileMenu->addAction(exitAct);

    //Populate Toolbar
    ui->basicOperationsToolBar->addAction(mirrorHorizontalAct);
    ui->basicOperationsToolBar->addAction(mirrorVerticalAct);
    ui->basicOperationsToolBar->addAction(luminescenceAct);
    ui->basicOperationsToolBar->addAction(quantizationAct);
    ui->basicOperationsToolBar->addAction(showHistogramAct);
    ui->basicOperationsToolBar->addAction(computeNegativeAct);

    ui->basicOperationsToolBar->addAction(enhanceBrightnessAct);
    ui->basicOperationsToolBar->addAction(decreaseBrightnessAct);

    ui->basicOperationsToolBar->addAction(enhanceContrastAct);
    ui->basicOperationsToolBar->addAction(decreaseContrastAct);

    ui->basicOperationsToolBar->addAction(resetImageAct);

    ui->basicOperationsToolBar->addAction(zoomInAct);
    ui->basicOperationsToolBar->addAction(zoomOutAct);

    ui->basicOperationsToolBar->addAction(rotateLeftAct);
    ui->basicOperationsToolBar->addAction(rotateRightAct);

    ui->basicOperationsToolBar->addAction(convolutionAct);
}

void ControlWindow::createActions()
{
    openFileAct = new QAction(tr("&Open file"), this);
    openFileAct->setShortcut(QKeySequence::New);
    openFileAct->setStatusTip(tr("Open an image file."));
    connect(openFileAct, &QAction::triggered, this, &ControlWindow::openFileDialog);

    saveFileAct = new QAction(tr("&Save file"), this);
    saveFileAct->setShortcut(QKeySequence::Save);
    saveFileAct->setStatusTip(tr("Save an image file"));
    connect(saveFileAct, &QAction::triggered, this, &ControlWindow::saveFile);

    saveFileAsAct = new QAction(tr("&Save file as"), this);
    saveFileAsAct->setShortcut(QKeySequence::SaveAs);
    saveFileAsAct->setStatusTip(tr("Save an image file as"));
    connect(saveFileAsAct, &QAction::triggered, this, &ControlWindow::saveFileAs);

    closeFileAct = new QAction(tr("&Close file"), this);
    //closeFileAct->setShortcut(QKeySequence::Cancel);
    closeFileAct->setStatusTip(tr("Close an image fil"));
    connect(closeFileAct, &QAction::triggered, this, &ControlWindow::closeFile);

    exitAct = new QAction(tr("&Exit"), this);
    exitAct->setShortcut(QKeySequence::Close);
    exitAct->setStatusTip(tr("Exit from editor"));
    connect(exitAct, &QAction::triggered, this, &ControlWindow::exit);


    mirrorHorizontalAct = new QAction(QIcon::fromTheme("object-flip-vertical"), tr("Mirror Horizontal"), this);
    mirrorHorizontalAct->setStatusTip(tr("Mirror the image horizontaly"));
    connect(mirrorHorizontalAct, &QAction::triggered, this, &ControlWindow::doMirrorHorizontal);

    mirrorVerticalAct = new QAction(QIcon::fromTheme("object-flip-horizontal"), tr("Mirror Vertical"), this);
    mirrorVerticalAct->setStatusTip(tr("Mirror the image vertically"));
    connect(mirrorVerticalAct, &QAction::triggered, this, &ControlWindow::doMirrorVertical);

    luminescenceAct = new QAction(QIcon::fromTheme("image-x-generic"), tr("Luminescence"), this);
    luminescenceAct->setStatusTip(tr("Compute Luminescence"));
    connect(luminescenceAct, &QAction::triggered, this, &ControlWindow::convertToGrayScale);

    quantizationAct = new QAction(QIcon::fromTheme("applications-graphics"), tr("Quantization"), this);
    quantizationAct->setStatusTip(tr("Shades quantization"));
    connect(quantizationAct, &QAction::triggered, this, &ControlWindow::doQuantization);

    showHistogramAct = new QAction(QIcon::fromTheme("utilities-system-monitor"), tr("Histogram"), this);
    showHistogramAct->setStatusTip(tr("Show Histogram"));
    connect(showHistogramAct, &QAction::triggered, this, &ControlWindow::showHistogram);

    computeNegativeAct = new QAction(QIcon::fromTheme("image-missing"), tr("Negative"), this);
    computeNegativeAct->setStatusTip(tr("Compute the negative of the image."));
    connect(computeNegativeAct, &QAction::triggered, this, &ControlWindow::doComputeNegative);

    enhanceBrightnessAct = new QAction(QIcon::fromTheme("go-up"), tr("Enhance Brightness"), this);
    enhanceBrightnessAct->setStatusTip(tr("Enhance Brightness"));
    connect(enhanceBrightnessAct, &QAction::triggered, this, &ControlWindow::enhanceBrightness);

    decreaseBrightnessAct = new QAction(QIcon::fromTheme("go-down"), tr("Decrease Brightness"), this);
    decreaseBrightnessAct->setStatusTip(tr("Decrease Brightness"));
    connect(decreaseBrightnessAct, &QAction::triggered, this, &ControlWindow::decreaseBrightness);

    enhanceContrastAct = new QAction(QIcon::fromTheme("view-sort-descending"), tr("Enhance Contrast"), this);
    enhanceContrastAct->setStatusTip(tr("Enhance Contrast"));
    connect(enhanceContrastAct, &QAction::triggered, this, &ControlWindow::enhanceContrast);

    decreaseContrastAct = new QAction(QIcon::fromTheme("view-sort-ascending"), tr("Decrease Contrast"), this);
    decreaseContrastAct->setStatusTip(tr("Decrease Contrast"));
    connect(decreaseContrastAct, &QAction::triggered, this, &ControlWindow::decreaseContrast);


    resetImageAct = new QAction(QIcon::fromTheme("view-restore"), tr("Reset Image"), this);
    resetImageAct->setStatusTip(tr("Reset Image"));
    connect(resetImageAct, &QAction::triggered, this, &ControlWindow::resetImage);

    zoomInAct = new QAction(QIcon::fromTheme("zoom-in"), tr("Zoom in"), this);
    zoomInAct->setStatusTip(tr("Zoom in"));
    connect(zoomInAct, &QAction::triggered, this, &ControlWindow::zoomIn);

    zoomOutAct = new QAction(QIcon::fromTheme("zoom-out"), tr("Zoom out"), this);
    zoomOutAct->setStatusTip(tr("Zoom out"));
    connect(zoomOutAct, &QAction::triggered, this, &ControlWindow::zoomOut);

    rotateRightAct = new QAction(QIcon::fromTheme("object-rotate-right"), tr("Rotate right."), this);
    rotateRightAct->setStatusTip(tr("Rotate right."));
    connect(rotateRightAct, &QAction::triggered, this, &ControlWindow::doRotateRight);

    rotateLeftAct = new QAction(QIcon::fromTheme("object-rotate-left"), tr("Rotate left."), this);
    rotateLeftAct->setStatusTip(tr("Rotate left."));
    connect(rotateLeftAct, &QAction::triggered, this, &ControlWindow::doRotateLeft);

    convolutionAct = new QAction(QIcon::fromTheme("sync-synchronizing"), tr("Convolution."), this);
    convolutionAct->setStatusTip(tr("Apply a convolution kernel to the image."));
    connect(convolutionAct, &QAction::triggered, this, &ControlWindow::openConvultionPopup);

    disableButtons();
}

void ControlWindow::openFileDialog()
{
    QString fileName;

    //fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Images (*.jpg *.jpeg)"));
    fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QStandardPaths::writableLocation(QStandardPaths::PicturesLocation),
                                            tr("Images (*.jpg *.jpeg)"));

    //statusLabel->setText(fileName);

    if(!fileName.isEmpty())
    {
        originalImage->setFileName(fileName);
        if(originalImage->loadImage(fileName))
        {
            originalView.setImage(originalImage);
            originalView.displayImage();
            originalView.show();

            copyImage->setFileName(fileName);
            copyImage->loadImage(fileName);
            copyView.setImage(copyImage);
            copyView.displayImage();
            copyView.show();

            enableButtons();
        }
        else
        {
            disableButtons();

            QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                     tr("Couldn't load image file %1").arg(originalImage->getFileName()));
        }
    }
}

void ControlWindow::saveFile()
{
    copyImage->saveFile();
}

void ControlWindow::saveFileAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File as"), copyImage->getFileName(), tr("Images (*.jpg)"));

    if(!fileName.isEmpty())
    {
        fileName = fileName + tr(".jpg");
        copyImage->saveAs(fileName);
    }
}


void ControlWindow::closeEvent(QCloseEvent *event)
{
    originalView.close();
    copyView.close();
}

void ControlWindow::enableButtons()
{
    mirrorHorizontalAct->setDisabled(false);
    mirrorVerticalAct->setDisabled(false);
    luminescenceAct->setDisabled(false);
    quantizationAct->setDisabled(false);
    showHistogramAct->setDisabled(false);
    enhanceBrightnessAct->setDisabled(false);
    decreaseBrightnessAct->setDisabled(false);
    enhanceContrastAct->setDisabled(false);
    decreaseContrastAct->setDisabled(false);
    resetImageAct->setDisabled(false);
    computeNegativeAct->setDisabled(false);
    zoomInAct->setDisabled(false);
    rotateRightAct->setDisabled(false);
    rotateLeftAct->setDisabled(false);
    convolutionAct->setDisabled(false);
    zoomOutAct->setDisabled(false);
}

void ControlWindow::disableButtons()
{

    mirrorHorizontalAct->setDisabled(true);
    mirrorVerticalAct->setDisabled(true);
    luminescenceAct->setDisabled(true);
    quantizationAct->setDisabled(true);
    showHistogramAct->setDisabled(true);
    enhanceBrightnessAct->setDisabled(true);
    decreaseBrightnessAct->setDisabled(true);
    enhanceContrastAct->setDisabled(true);
    decreaseContrastAct->setDisabled(true);
    resetImageAct->setDisabled(true);
    computeNegativeAct->setDisabled(true);
    zoomInAct->setDisabled(true);
    rotateRightAct->setDisabled(true);
    rotateLeftAct->setDisabled(true);
    convolutionAct->setDisabled(true);
    zoomOutAct->setDisabled(true);
}

void ControlWindow::resetImage()
{
    copyImage->loadImage(originalImage->getFileName());
    copyView.displayImage();
}

void ControlWindow::doMirrorHorizontal()
{
    copyImage->horizontalMirroring();
    copyView.displayImage();
}

void ControlWindow::doMirrorVertical()
{
    copyImage->verticalMirroring();
    copyView.displayImage();
}

void ControlWindow::convertToGrayScale()
{
    copyImage->calculateLuminance();
    copyView.displayImage();
    //copyImage->computeHistogram();
    //copyImage->printHistogram();
}

void ControlWindow::doQuantization()
{
    QuantizationDialog* dialog = new QuantizationDialog;
    dialog->setWindowTitle(tr("Shades"));
    dialog->exec();
    QSpinBox* spbox =  dialog->findChild<QSpinBox*>("nShades");

    qDebug("Shades %i", spbox->value());

    if(dialog->result() == QDialog::Accepted)
    {
        copyImage->imageQuantization(spbox->value());
        copyView.displayImage();
    }
}

void ControlWindow::showHistogram()
{
    HistogramView* histoView = new HistogramView;
    convertToGrayScale();
    histoView->showHistogram(copyImage->getHistogram());
    histoView->show();
    showHistogramAct->setDisabled(true);

    connect(histoView, SIGNAL(windowClosed()), this, SLOT(histogramViewClosed()));
}

void ControlWindow::doComputeNegative()
{
    copyImage->computeNegative();
    copyView.displayImage();
}

void ControlWindow::enhanceBrightness()
{
    copyImage->setBrightness(+50);
    copyView.displayImage();
}

void ControlWindow::decreaseBrightness()
{
    copyImage->setBrightness(-50);
    copyView.displayImage();
}

void ControlWindow::enhanceContrast()
{
    copyImage->setContrast(1.5);
    copyView.displayImage();
}

void ControlWindow::decreaseContrast()
{
    copyImage->setContrast(.5);
    copyView.displayImage();
}

void ControlWindow::zoomIn()
{
    copyImage->zoomIn();
    copyView.displayImage();
}

void ControlWindow::zoomOut()
{
    copyImage->zoomOut();
    copyView.displayImage();
}

void ControlWindow::doRotateRight()
{
    copyImage->rotate(right);
    copyView.displayImage();
}

void ControlWindow::doRotateLeft()
{
    copyImage->rotate(left);
    copyView.displayImage();
}

void ControlWindow::openConvultionPopup()
{
    KernelView* kernelPopup = new KernelView();

    connect(kernelPopup, SIGNAL(kernelSet(ConvolutionKernel*)), this, SLOT(setKernel(ConvolutionKernel*)));

    kernelPopup->show();
}

void ControlWindow::setKernel(ConvolutionKernel *convkernel)
{
    convertToGrayScale();
    copyImage->setConvolutionKernel(convkernel);
    copyImage->doConvolution();
    copyView.displayImage();
}

void ControlWindow::histogramViewClosed()
{
    showHistogramAct->setDisabled(false);
}

void ControlWindow::closeFile()
{
    originalView.close();
    copyView.close();
    disableButtons();
}

void ControlWindow::exit()
{
    this->close();
}
