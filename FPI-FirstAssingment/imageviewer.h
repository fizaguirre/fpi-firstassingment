#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QWidget>
#include "imagefile.h"

namespace Ui {
class ImageViewer;
}

class ImageViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ImageViewer(QWidget *parent = 0);
    ~ImageViewer();

    void setImage(ImageFile* _image);
    void displayImage();


private:
    Ui::ImageViewer *ui;

    ImageFile* image;
};

#endif // IMAGEVIEWER_H
