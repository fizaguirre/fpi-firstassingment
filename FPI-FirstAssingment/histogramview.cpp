#include "histogramview.h"
#include "ui_histogramview.h"

#include <qimage.h>

#define WIN_HIST_HEIGHT 256
#define WIN_HIST_WIDTH 256
#define HISTO_SIZE 256

HistogramView::HistogramView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HistogramView)
{
    ui->setupUi(this);
}

HistogramView::~HistogramView()
{
    delete ui;
}

/*
void HistogramView::showHistogram(uchar *histogram)
{
    QImage histoImage(WIN_HIST_WIDTH, WIN_HIST_HEIGHT, QImage::Format_RGB888);

    for(int histoIndex = 0; histoIndex < HISTO_SIZE; histoIndex++)
    {
        if(histogram[histoIndex] > 255)
            histogram[histoIndex] = 255;

        for(int histoImageHeight = histogram[histoIndex]; histoImageHeight >= 0; histoImageHeight--)
            histoImage.scanLine(histoImageHeight)[histoIndex] = (uchar)255;
    }

    ui->histogramLabel->setPixmap(QPixmap::fromImage(histoImage)); //Set an histogram image
}
*/

void HistogramView::showHistogram(uchar *histogram)
{
    QImage histoImage(WIN_HIST_WIDTH, WIN_HIST_HEIGHT, QImage::Format_RGB888);
    histoImage.fill(QColor(Qt::white).rgb());

    normalizedHistogram = normalizeHistogram(histogram);

    for(int histoIndex = 0; histoIndex < HISTO_SIZE; histoIndex++)
    {
        qDebug("Histogram[%i]: %i", histoIndex, normalizedHistogram[histoIndex]);
        if(normalizedHistogram[histoIndex] > 255)
            normalizedHistogram[histoIndex] = 255;

        /*
        for(int histoImageHeight = histogram[histoIndex], count = 255; histoImageHeight >= 0; histoImageHeight--, count--)
            histoImage.setPixel(histoIndex, count, qRgb(0,0,255));

         */
        for(int histoImageHeight = normalizedHistogram[histoIndex], count = 255 ; histoImageHeight >= 0; histoImageHeight--, count--)
        {
            histoImage.scanLine(count)[histoIndex*3] = 0;
            histoImage.scanLine(count)[histoIndex*3 + 1] = 0;
            histoImage.scanLine(count)[histoIndex*3 + 2] = 255;
        }

    }

    ui->histogramLabel->setPixmap(QPixmap::fromImage(histoImage)); //Set an histogram image
}

uchar *HistogramView::normalizeHistogram(uchar *h)
{
    uchar* nhistogram = (uchar*)calloc(HISTO_SIZE, sizeof(uchar));

    uint maxvalue = 0;
    for(int i = 0; i < HISTO_SIZE; i++)
    {
        if(h[i] > maxvalue)
            maxvalue = h[i];
    }

    for(int i = 0; i < HISTO_SIZE; i++)
        nhistogram[i] = (uchar)(((float)h[i]/(float)maxvalue) * (float)HISTO_SIZE);

    return nhistogram;
}

void HistogramView::HistogramView::closeEvent(QCloseEvent *event)
{
    emit windowClosed();
}
