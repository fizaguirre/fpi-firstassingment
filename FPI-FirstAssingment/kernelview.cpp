#include "kernelview.h"
#include "ui_kernelview.h"

KernelView::KernelView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KernelView)
{
    ui->setupUi(this);

    dialogKernel = new ConvolutionKernel(0,3,3);

    on_kernelsListComboBox_currentIndexChanged(ui->kernelsListComboBox->currentText());
}

KernelView::~KernelView()
{
    delete ui;
}

ConvolutionKernel *KernelView::getKernel()
{
    return dialogKernel;
}

void KernelView::on_convolutionButtonBox_clicked(QAbstractButton *button)
{
    if((QPushButton*)button == ui->convolutionButtonBox->button(QDialogButtonBox::Ok))
    {
        dialogKernel->setValuePosition(0,0,ui->ker00->value());
        dialogKernel->setValuePosition(0,1,ui->ker01->value());
        dialogKernel->setValuePosition(0,2,ui->ker02->value());

        dialogKernel->setValuePosition(1,0,ui->ker10->value());
        dialogKernel->setValuePosition(1,1,ui->ker11->value());
        dialogKernel->setValuePosition(1,2,ui->ker12->value());

        dialogKernel->setValuePosition(2,0,ui->ker20->value());
        dialogKernel->setValuePosition(2,1,ui->ker21->value());
        dialogKernel->setValuePosition(2,2,ui->ker22->value());

        dialogKernel->printKernel();

        setKernelFilter(ui->kernelsListComboBox->currentText());

        emit kernelSet(getKernel());
    }
}

void KernelView::setKernelFilter(QString selectedItem)
{
    bool equal = (selectedItem == tr("Gaussian"));
    if(selectedItem == "Gaussian")
        dialogKernel->setFilterType(Gaussian);
    else if(selectedItem == tr("Laplacian"))
        dialogKernel->setFilterType(Laplacian);
    else if(selectedItem == tr("Generic High Pass"))
        dialogKernel->setFilterType(GenericHighpass);
    else if(selectedItem == tr("Prewitt Hx"))
        dialogKernel->setFilterType(PrewittHx);
    else if(selectedItem == tr("Prewitt Hy Hx"))
        dialogKernel->setFilterType(PrewittHyHx);
    else if(selectedItem == tr("Sobel Hx"))
        dialogKernel->setFilterType(SobelHx);
    else if(selectedItem == tr("Sobel Hy"))
        dialogKernel->setFilterType(SobelHy);
}

void KernelView::on_kernelsListComboBox_currentIndexChanged(const QString &arg1)
{
    setKernelFilter(arg1);

    switch (dialogKernel->getFilterType())
    {
    case Gaussian:
        ui->ker00->setValue(0.0625); ui->ker01->setValue(0.125); ui->ker20->setValue(0.0625);
        ui->ker10->setValue(0.125); ui->ker11->setValue(0.25); ui->ker21->setValue(0.125);
        ui->ker20->setValue(0.0625); ui->ker21->setValue(0.125); ui->ker22->setValue(0.0625);
        break;
    case Laplacian:
        ui->ker00->setValue(0.0); ui->ker01->setValue(-1.0); ui->ker20->setValue(0.0);
        ui->ker10->setValue(-1.0); ui->ker11->setValue(4.0); ui->ker21->setValue(-1.0);
        ui->ker20->setValue(0.0); ui->ker21->setValue(-1.0); ui->ker22->setValue(0.0);
        break;
    case GenericHighpass:
        ui->ker00->setValue(-1.0); ui->ker01->setValue(-1.0); ui->ker20->setValue(-1.0);
        ui->ker10->setValue(-1.0); ui->ker11->setValue(8.0); ui->ker21->setValue(-1.0);
        ui->ker20->setValue(-1.0); ui->ker21->setValue(-1.0); ui->ker22->setValue(-1.0);
        break;
    case PrewittHx:
        ui->ker00->setValue(-1.0); ui->ker01->setValue(0.0); ui->ker20->setValue(1.0);
        ui->ker10->setValue(-1.0); ui->ker11->setValue(0.0); ui->ker21->setValue(1.0);
        ui->ker20->setValue(-1.0); ui->ker21->setValue(0.0); ui->ker22->setValue(1.0);
        break;
    case PrewittHyHx:
        ui->ker00->setValue(-1.0); ui->ker01->setValue(-1.0); ui->ker20->setValue(-1.0);
        ui->ker10->setValue(0.0); ui->ker11->setValue(0.0); ui->ker21->setValue(0.0);
        ui->ker20->setValue(1.0); ui->ker21->setValue(1.0); ui->ker22->setValue(1.0);
        break;
    case SobelHx:
        ui->ker00->setValue(-1.0); ui->ker01->setValue(0.0); ui->ker20->setValue(1.0);
        ui->ker10->setValue(-2.0); ui->ker11->setValue(0.0); ui->ker21->setValue(2.0);
        ui->ker20->setValue(-1.0); ui->ker21->setValue(0.0); ui->ker22->setValue(1.0);
        break;
    case SobelHy:
        ui->ker00->setValue(-1.0); ui->ker01->setValue(-2.0); ui->ker20->setValue(-1.0);
        ui->ker10->setValue(0.0); ui->ker11->setValue(0.0); ui->ker21->setValue(0.0);
        ui->ker20->setValue(1.0); ui->ker21->setValue(2.0); ui->ker22->setValue(0.0);
        break;
        default:
            break;
    }
}
