#include "quantizationdialog.h"
#include "ui_quantizationdialog.h"

QuantizationDialog::QuantizationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuantizationDialog)
{
    ui->setupUi(this);
}

QuantizationDialog::~QuantizationDialog()
{
    delete ui;
}
