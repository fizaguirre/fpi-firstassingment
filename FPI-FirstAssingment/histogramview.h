#ifndef HISTOGRAMVIEW_H
#define HISTOGRAMVIEW_H

#include <QWidget>

namespace Ui {
class HistogramView;
}

class HistogramView : public QWidget
{
    Q_OBJECT

public:
    explicit HistogramView(QWidget *parent = 0);
    ~HistogramView();

    void showHistogram(uchar* histogram);

private:
    Ui::HistogramView *ui;

    uchar* normalizedHistogram;

    uchar* normalizeHistogram(uchar* h);

    void closeEvent(QCloseEvent *event);

signals:
    void windowClosed();
};

#endif // HISTOGRAMVIEW_H
