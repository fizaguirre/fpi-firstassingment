#ifndef CONTROLWINDOW_H
#define CONTROLWINDOW_H

#include <QMainWindow>
#include <QLabel>

#include "imagefile.h"
#include "imageviewer.h"
#include "convolutionkernel.h"

namespace Ui {
class ControlWindow;
}

class ControlWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ControlWindow(QWidget *parent = 0);
    ~ControlWindow();

private:
    Ui::ControlWindow *ui;

    //Variables
    ImageFile* originalImage;
    ImageFile* copyImage;

    QLabel* statusLabel;

    //Menus
    QMenu* fileMenu;

    //Actions
    QAction* openFileAct;
    QAction* saveFileAct;
    QAction* saveFileAsAct;
    QAction* closeFileAct;
    QAction* exitAct;

    QAction* resetImageAct;

    QAction* mirrorHorizontalAct;
    QAction* mirrorVerticalAct;
    QAction* luminescenceAct;
    QAction* quantizationAct;
    QAction* showHistogramAct;
    QAction* computeNegativeAct;

    QAction* enhanceBrightnessAct;
    QAction* decreaseBrightnessAct;

    QAction* enhanceContrastAct;
    QAction* decreaseContrastAct;

    QAction* zoomInAct;
    QAction* zoomOutAct;

    QAction* rotateRightAct;
    QAction* rotateLeftAct;

    QAction* convolutionAct;

    //Windows
    ImageViewer originalView;
    ImageViewer copyView;

    //Methods
    void createMenus();
    void createActions();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void enableButtons();
    void disableButtons();

    void resetImage();

    void openFileDialog();
    void saveFile();
    void saveFileAs();
    void closeFile();
    void exit();

    void doMirrorHorizontal();
    void doMirrorVertical();
    void convertToGrayScale();
    void doQuantization();
    void showHistogram();
    void doComputeNegative();

    void enhanceBrightness();
    void decreaseBrightness();
    void enhanceContrast();
    void decreaseContrast();

    void zoomIn();
    void zoomOut();

    void doRotateRight();
    void doRotateLeft();

    void openConvultionPopup();

public slots:
    void setKernel(ConvolutionKernel* convkernel);
    void histogramViewClosed();

};

#endif // CONTROLWINDOW_H
